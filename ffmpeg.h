#ifndef FFMPEG_H
#define FFMPEG_H

typedef struct FF_Video FF_Video;
typedef struct FF_Audio FF_Audio;
typedef struct FFmpeg FFmpeg;

struct FF_Video
{
    const AVCodecParameters *codec_par;
    const AVCodec *codec;
    int stream_index;
};

struct FF_Audio
{
    const AVCodecParameters *codec_par;
    const AVCodec *codec;
    int stream_index;
};

struct FFmpeg
{
    AVFormatContext *format_ctx;
    AVCodecContext *codec_ctx;
    Eina_Array *videos;
    Eina_Array *audios;
    int video_index;
    int audio_index;
};


FFmpeg *ff_new(void);

void ff_free(FFmpeg *ff);

int ff_file_open(FFmpeg *ff, const char *file);

void ff_file_close(FFmpeg *ff);

void ff_size_get(FFmpeg *ff, int *width, int *height);

double ff_len_get(FFmpeg *ff);

int _ff_fps_get(FFmpeg *ff, int *n, int *d);

int ff_fps_num_get(FFmpeg *ff);

int ff_fps_den_get(FFmpeg *ff);

double ff_fps_get(FFmpeg *ff);

int ff_video_handled(FFmpeg *ff);

int ff_audio_handled(FFmpeg *ff);

int ff_format_get(FFmpeg *ff);

int ff_video_channel_count(FFmpeg *ff);

void ff_video_channel_set(FFmpeg *ff, int channel);

int ff_video_channel_get(FFmpeg *ff);

int ff_audio_channel_count(FFmpeg *ff);

void ff_audio_channel_set(FFmpeg *ff, int channel);

int ff_audio_channel_get(FFmpeg *ff);

#endif /* FFMPEG_H */
